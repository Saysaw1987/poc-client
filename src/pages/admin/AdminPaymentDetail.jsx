import React, { useState, useEffect } from "react";
import { ToastContainer, toast, Flip } from "react-toastify";
import { toastProps } from "../../data/toastProps/toastProps";
import UserDetailsDisplay from "../../components/UserDetailsDisplay";
import InfoBox from "../../components/infoDisplayBoxes/InfoBox";
import { parseTokenFromLocalStorage } from "../../functions/parseTokenFromLocalStorage";

const AdminPaymentDetail = () => {
  const selectedPayment = JSON.parse(localStorage.getItem("paymentId"));
  const displayPaymentId = selectedPayment.slice(0, 8);

  // Sets payment details from API call
  const [name, setName] = useState();
  const [email, setEmail] = useState();
  const [company, setCompany] = useState();
  const [city, setCity] = useState();
  const [country, setCountry] = useState();
  const [phone, setPhone] = useState();
  const [baseComp, setBaseComp] = useState(0);
  const [currency, setCurrency] = useState();
  const [payrollDate, setPayrollDate] = useState("");
  const [hours, setHours] = useState(0);
  const [requiredHours, setRequiredHours] = useState();
  const [requestDate, setRequestDate] = useState("");
  const [adminNotes, setAdminNotes] = useState("no notes");
  const [paymentAmount, setPaymentAmount] = useState(0);
  const [invoiceDesc, setInvoiceDesc] = useState([]);

  // Sets items from localStorage user profile upon first render
  useEffect(() => {
    fetch(
      `${process.env.REACT_APP_API}/admin/payment-user-details/${selectedPayment}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${parseTokenFromLocalStorage()}`,
        },
      }
    )
      .then((response) => response.json())
      .then((data) => {
        setName(`${data[0].firstName} ${data[0].lastName}`);
        setCompany(data[0].company);
        setEmail(data[0].email);
        setCity(data[0].city);
        setCountry(data[0].country);
        setPhone(data[0].phone);
        setPayrollDate(data[0].payrollDate.slice(0, 10));
        setRequestDate(data[0].requestDate.slice(0, 10));
        setHours(data[0].hoursWorked);
        setRequiredHours(data[0].requiredHours);
        setCurrency(data[0].currency);
        setBaseComp(data[0].baseCompensation);
        setPaymentAmount(data[0].totalPayment);

        if (data[0].bonus === 0) {
          // Sets payment desc to local storage
          localStorage.setItem(
            "paymentDesc",
            JSON.stringify(data[0].description)
          );
          // Parses paymentDescription from local storage
          let description = [JSON.parse(localStorage.getItem("paymentDesc"))];

          // Converts payment description so it can be mapped in table
          let descriptionArray = Object.values(description[0]);
          setInvoiceDesc(descriptionArray);
        }

        if (data[0].adminNotes !== null) setAdminNotes(data[0].adminNotes);
      })
      .catch((error) => {
        toast.error("Error: Something went wrong.", toastProps);
      });
  }, [selectedPayment]);

  return (
    <main className="page-layout">
      <h1 className="page-heading">Payment Detail</h1>
      <div className="centered-text-heading">
        <h2 className="section-heading">
          Payment details for payment id: {displayPaymentId}
        </h2>
        <p className="para-text">
          This request was submitted on: {requestDate}
        </p>
      </div>
      {/* User Details Section */}
      {/* User Details Section */}
      <UserDetailsDisplay
        name={name}
        company={company}
        email={email}
        city={city}
        country={country}
        phone={phone}
      />
      {/* Payment Details Section */}
      <div className="main-form">
        <div className="main-form-header">Payment Request Details</div>
        <div className="top-form-container">
          {/* Status section */}
          <InfoBox
            title="Status:"
            content="approved"
            className="form-item"
            id="paid"
          />

          {/* Payment date section */}
          <InfoBox
            title="Payment Date:"
            content={payrollDate}
            className="form-item"
          />

          {/* Total hours section */}
          <InfoBox
            title="Hours Worked:"
            content={hours}
            className="form-item"
          />
        </div>

        <div className="top-form-container">
          {/* Required hours section */}
          <InfoBox
            title="Expected Hours:"
            content={requiredHours}
            className="form-item"
          />

          {/* Base comp section */}
          <InfoBox
            title="Base Compensation Rate:"
            content={`$${baseComp} ${currency}`}
            className="form-item"
          />

          {/* Total payment section */}
          <InfoBox
            title="Total Payment Amount:"
            content={`$${paymentAmount} ${currency}`}
            className="form-item"
          />
        </div>
      </div>
      {/* Work Description Section */}
      <div className="main-form">
        <div className="main-form-header">Description of Work</div>
        <div className="bottom-form-container">
          {/* Maps timesheet items */}
          <table className="payment-request-table">
            <thead>
              <tr>
                <th className="table-heading" id="table-hours">
                  Hours:
                </th>
                <th className="table-heading" id="table-project">
                  Client/Project:
                </th>
                <th className="table-heading" id="table-description">
                  Description:
                </th>
              </tr>
            </thead>
            <tbody>
              {invoiceDesc.map((item) => (
                <React.Fragment key={item.itemId}>
                  <tr>
                    <td>{item.itemHours}</td>
                    <td>{item.itemProject}</td>
                    <td>{item.itemDescription}</td>
                  </tr>
                </React.Fragment>
              ))}
            </tbody>
          </table>
        </div>
      </div>
      {/* Admin Notes Section */}
      <div className="main-form">
        <div className="main-form-header">Administration Notes</div>
        <div className="top-form-container">
          <p className="para-text">{adminNotes}</p>
        </div>
      </div>
      <ToastContainer transition={Flip} />
    </main>
  );
};

export default AdminPaymentDetail;
